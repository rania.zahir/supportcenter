using System.Diagnostics;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SC.BL.Domain;

namespace SC.DAL.EF;

public class SupportCenterDbContext : IdentityDbContext<IdentityUser>
{
    public DbSet<Ticket> Tickets { get; set; }
    public DbSet<HardwareTicket> HardwareTickets { get; set; }
    public DbSet<TicketResponse> TicketResponses { get; set; }

    public SupportCenterDbContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
            optionsBuilder.UseSqlite("Data Source=SupportCenterDb_EFCodeFirst.db");
            
        // configure logging: write to debug output window
        optionsBuilder.LogTo(message => Debug.WriteLine(message), LogLevel.Information);
            
        // configure lazy-loading
        optionsBuilder.UseLazyLoadingProxies(false); // 'false' to disable -> default 'true'
    }
   
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        // 'Ticket.State' as index
        modelBuilder.Entity<Ticket>().HasIndex(t => t.State);
            
        // foreign key-name "TicketFK" in TicketResponse to Ticket
        modelBuilder.Entity<TicketResponse>().Property<int>("TicketFK"); // shadow property
        modelBuilder.Entity<TicketResponse>()
            .HasOne(tr => tr.Ticket)
            .WithMany(t => t.Responses)
            .HasForeignKey("TicketFK")
            .IsRequired();
    }
    
     public void Seed()
    {
        //t
        /*IdentityUser kennethUser = new IdentityUser
        {
            Id = "1", // Replace with actual Id from IdentityUser
            Email = "kenneth@kdg.be",
            // Add other properties
        };

        // Associate existing tickets with the user
        foreach (var ticket in Tickets)
        {
            ticket.Maintainer = kennethUser;
        }*/
        //t
        // Create first ticket with three responses
        Ticket t1 = new Ticket()
        {
            AccountId = 1,
            Text = "Ik kan mij niet aanmelden op de webmail",
            DateOpened = new DateTime(2021, 9, 9, 13, 5, 59),
            State = TicketState.Open,
            Maintainer = Users.SingleOrDefault(u => u.UserName == "kenneth@kdg.be"),
            Responses = new List<TicketResponse>()
        };
        Tickets.Add(t1);
        Ticket tt = new Ticket()
        {
            AccountId = 1,
            Text = "Mijn dockstation is kapot",
            DateOpened = new DateTime(2024, 3, 2, 7, 40, 20),
            State = TicketState.Open,
            Maintainer = Users.SingleOrDefault(u => u.UserName == "kenneth@kdg.be"),
            Responses = new List<TicketResponse>()
        };
        Tickets.Add(tt);

        TicketResponse t1r1 = new TicketResponse()
        {
            Ticket = t1,
            Text = "Account was geblokkeerd",
            Date = new DateTime(2021, 9, 9, 13, 24, 48),
            IsClientResponse = false
        };
        t1.Responses.Add(t1r1);
            
        TicketResponse t1r2 = new TicketResponse() {
            Ticket = t1,
            Text = "Account terug in orde en nieuw paswoord ingesteld",
            Date = new DateTime(2021, 9, 9, 13, 29, 11),
            IsClientResponse = false
        };
        t1.Responses.Add(t1r2); 

        TicketResponse t1r3 = new TicketResponse() {
            Ticket = t1,
            Text = "Aanmelden gelukt en paswoord gewijzigd",
            Date = new DateTime(2021, 9, 10, 7, 22, 36),
            IsClientResponse = true
        };
        t1.Responses.Add(t1r3);
        t1.State = TicketState.Closed;

        // Create second ticket with one response
        Ticket t2 = new Ticket() {
            AccountId = 1,
            Text = "Geen internetverbinding",
            DateOpened = new DateTime(2021, 11, 5, 9, 45, 13),
            State = TicketState.Open,
            Maintainer = Users.SingleOrDefault(u => u.UserName == "kenneth@kdg.be"),
            Responses = new List<TicketResponse>()
        };
        Tickets.Add(t2);

        TicketResponse t2r1 = new TicketResponse()
        {
            Ticket = t2,
            Text = "Controleer of de kabel goed is aangesloten",
            Date = new DateTime(2021, 11, 5, 11, 25, 42),
            IsClientResponse = false
        };
        t2.Responses.Add(t2r1);
        t2.State = TicketState.Answered;

        // Create hardware ticket without response
        HardwareTicket ht1 = new HardwareTicket()
        {
            AccountId = 2,
            Text = "Blue screen!",
            DateOpened = new DateTime(2021, 12, 14, 19, 5, 2),
            State = TicketState.Open,
            Maintainer = Users.SingleOrDefault(u => u.UserName == "rania@kdg.be"),
            DeviceName = "PC-123456"
        };
        Tickets.Add(ht1);

        // Save the changes in the context to the database
        SaveChanges();
            
        // Clear ChangeTracker
        // otherwise this data stays tracked and will have to be read from the db when needed!
        ChangeTracker.Clear();
    }
    
    public bool CreateDatabase(bool dropDatabase = false)
    {
        if (dropDatabase)
            this.Database.EnsureDeleted();

        return this.Database.EnsureCreated();
    }
}