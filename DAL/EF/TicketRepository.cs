using Microsoft.EntityFrameworkCore;
using SC.BL.Domain;

namespace SC.DAL.EF;

public class TicketRepository : ITicketRepository
{
    private readonly SupportCenterDbContext _ctx;
        
    public TicketRepository(SupportCenterDbContext scDbContext)
    {
        _ctx = scDbContext;
    }
        
    public Ticket CreateTicket(Ticket ticket)
    {
        _ctx.Tickets.Add(ticket);
        _ctx.SaveChanges();

        return ticket; // 'TicketNumber' has been created by the database!
    }

    public IEnumerable<Ticket> ReadTickets()
    {
        IEnumerable<Ticket> tickets = _ctx.Tickets
            .Include(t => t.Responses)
            .AsEnumerable();

        return tickets;
    }

    public Ticket ReadTicket(int ticketNumber)
    {
        Ticket ticket = _ctx.Tickets
            .Include(t => t.Responses)
            .Include(t => t.Maintainer)  // Include Maintainer here
            .FirstOrDefault(t => t.TicketNumber == ticketNumber);
        return ticket;
        /*Ticket ticket = _ctx.Tickets
            .Include(t => t.Maintainer) // Include Maintainer property
            .SingleOrDefault(t => t.TicketNumber == ticketNumber);

        return ticket;*/
    }

    public void UpdateTicket(Ticket ticket)
    {
        _ctx.Tickets.Update(ticket);
        _ctx.SaveChanges();
    }

    public void DeleteTicket(int ticketNumber)
    {
        Ticket ticket = _ctx.Tickets.Find(ticketNumber);
        _ctx.Tickets.Remove(ticket);
        _ctx.SaveChanges();

    }

    public IEnumerable<TicketResponse> ReadTicketResponsesOfTicket(int ticketNumber)
    {
        // IEnumerable<TicketResponse> responses = _ctx.TicketResponses
        //     .Where(r => r.Ticket.TicketNumber == ticketNumber)
        //     .AsEnumerable();
        // return responses;
            
        // example of 'explicit loading'
        Ticket ticket = _ctx.Tickets.Find(ticketNumber);
        _ctx.Entry<Ticket>(ticket).Collection(t => t.Responses).Load();

        return ticket.Responses;
    }

    public TicketResponse CreateTicketResponse(TicketResponse response)
    {
        _ctx.TicketResponses.Add(response);
        _ctx.SaveChanges();

        return response; // 'Id' has been created by the database!
    }
}