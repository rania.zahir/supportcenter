using System.ComponentModel.DataAnnotations;
using SC.BL.Domain;
using SC.DAL;

namespace SC.BL;

public class TicketManager : ITicketManager
{
    private readonly ITicketRepository _repo;

    public TicketManager(ITicketRepository ticketRepository)
    {
        _repo = ticketRepository;
    }

    public IEnumerable<Ticket> GetTickets()
    {
        return _repo.ReadTickets();
    }

    public Ticket GetTicket(int ticketNumber)
    {
        return _repo.ReadTicket(ticketNumber);
    }
        
    public Ticket AddTicket(int accountId, string question)
    {
        Ticket t = new Ticket()
        {
            AccountId = accountId,
            Text = question,
            DateOpened = DateTime.Now,
            State = TicketState.Open,
        };
        return this.AddTicket(t);
    }

    public Ticket AddTicket(int accountId, string device, string problem)
    {
        Ticket t = new HardwareTicket()
        {
            AccountId = accountId,
            Text = problem,
            DateOpened = DateTime.Now,
            State = TicketState.Open,
            DeviceName = device
        };
        return this.AddTicket(t);
    }

    /*private Ticket AddTicket(Ticket ticket)
    {
        this.Validate(ticket);
        return _repo.CreateTicket(ticket);
    }*/

    public void ChangeTicket(Ticket ticket)
    {
        this.Validate(ticket);
        _repo.UpdateTicket(ticket);
    }

    public void RemoveTicket(int ticketNumber)
    {
        _repo.DeleteTicket(ticketNumber);
    }

    public IEnumerable<TicketResponse> GetTicketResponses(int ticketNumber)
    {
        return _repo.ReadTicketResponsesOfTicket(ticketNumber);
    }

    public TicketResponse AddTicketResponse(int ticketNumber, string response, bool isClientResponse)
    {
        Ticket ticketToAddResponseTo = this.GetTicket(ticketNumber);
        if (ticketToAddResponseTo != null)
        {
            // Create response
            TicketResponse newTicketResponse = new TicketResponse();
            newTicketResponse.Date = DateTime.Now;
            newTicketResponse.Text = response;
            newTicketResponse.IsClientResponse = isClientResponse;
            newTicketResponse.Ticket = ticketToAddResponseTo;

            // Add response to existing responses of ticket
            var responses = this.GetTicketResponses(ticketNumber);

            if (responses != null)
                ticketToAddResponseTo.Responses = responses.ToList();
            else
                ticketToAddResponseTo.Responses = new List<TicketResponse>();

            ticketToAddResponseTo.Responses.Add(newTicketResponse);

            // Change state of ticket
            if (isClientResponse)
                ticketToAddResponseTo.State = TicketState.ClientAnswer;
            else
                ticketToAddResponseTo.State = TicketState.Answered;

            // Save changes to repository
            this.Validate(newTicketResponse);
            this.Validate(ticketToAddResponseTo);
            _repo.CreateTicketResponse(newTicketResponse);
            _repo.UpdateTicket(ticketToAddResponseTo);

            return newTicketResponse;
        }
        else
            throw new ArgumentException("Ticket with number '" + ticketNumber + "' not found!");
    }

    private void Validate(Ticket ticket)
    {
        List<ValidationResult> errors = new List<ValidationResult>();

        bool valid = Validator.TryValidateObject(ticket, new ValidationContext(ticket), errors, validateAllProperties: true);

        if (!valid)
            throw new ValidationException("Ticket not valid!");
    }

    private void Validate(TicketResponse response)
    {
        Validator.ValidateObject(response, new ValidationContext(response), validateAllProperties: true);
    }
    //n
    public Ticket AddTicket(Ticket newTicket)
    {
        this.Validate(newTicket);
        return _repo.CreateTicket(newTicket);
    }

}