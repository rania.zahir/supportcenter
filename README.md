## SPRINT 7 

## WEB API : PUT 
op de detail pagina is het attribuut in kwestie dat aangepast kan worden de 
status van het ticket. als men de beheerder van de ticket of admin is kan hij/zij
de status wijzigen en deze dus op close zetten. Vervolgens word de pagina/ticket
automatisch gerefresht/geupdatet naar de nieuwe status.
Indien men geen beheerder of admin is kan men de status dus niet wijzigen en 
als gevolg niet op de knop kan klikken waardoor de wijzigingen dus niet gemaakt worden.

## USERS
mail:rania@kdg.be ww:Rania1? => user

mail:kenneth@kdg.be ww:Kenneth1? => admin

mail:annick@kdg.be ww:Annick1? => user

mail:john@kdg.be ww:John1? => user

mail:sophie@kdg.be ww:Sophie1? => user

# Eerste Api Request (MVC_TESTS.HTTP)
## Create new response for ticket without authentication (Expect to fail)
````
POST https://localhost:7151/api/TicketResponses HTTP/1.1
Content-Type: application/json

{"ticketNumber":2,"responseText":"een nieuw antwoord","isClientResponse":false}
````

response :
````
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8
Date: Sun, 24 Mar 2024 08:14:50 GMT
Server: Kestrel
Transfer-Encoding: chunked

"Unauthorized: User is not authorized to perform this action"
````

# Tweede Api Request (MVC_TESTS.HTTP)
## Create new response for ticket as authenticated user (Expect to succeed)
````
POST https://localhost:7151/api/TicketResponses HTTP/1.1
Content-Type: application/json
Cookie: .AspNetCore.Identity.Application=CfDJ8NPIZeV348VBlhusYlZrZx8ehVjzrgYeniOJMUzj8ZfoLVVuKI2flomPzxRCBqOvhZxm8Dr7NY5bo3q9e2vsXYAVtRUUcX4yXpsVxDxvH_JeWnqiPUL2nB_DlL9bLJhunUjIJFwg5ZAAReBmzsTS6Crx8cFVxAl4J72kz07oFQbT1YZ3gUPRRXTmiWQyIF9MEP6FidsPeSzXg6LhRJwh3w_sAANS5WIoARjVgExzmG9_77_dw5mGW3tv7xbrqJ57qmJstJgLhS2NoUeZoPmjpCrRxi2SdiPNHJSQ9sursa6I4-bFt5wdpkt_z4f81wz8k26-7OgNLVxoOePlhKZrYKDSCT3eGN6iv5aL5O7K3ABr53NK5jsJahUjuEOCZo45Wxlp6HH0coGsaHjeHxwVN1fZTkm1uh4CsjHggUGjNod5SzcuH2ERApzqsdCfpa-96Zz5mQrMznAf-hqU1KW8RXPDgD8vcL5du09RXzMki_gU57UhikWqcAjklZSozUgK1mqzsCJeSDQfwqUmClTlbRni0bleTu_QG52DG-TUHpuCLEnWAiSjIYTMVq6l1pfmqjViBmPdGiI-M3zMOtw9DeW6j6_eD6m0K7kl0hD_Btk0ndsQhQl4mDYyjgmEYYfuG6NwH4Al1yorBUWI5F6xjXEYctRpGOCViaSk0Wo8Wi1lNOfyCButWiekudmbToPJ5WkRwH9TNVfRH4JQaoiHrrWS6JR5kLiLjhSbL5mXsR4qCM6CD-DH6L2jHpgUGsUZRwIkWU-2v5akO-_qb4xYtdaAmYYWoyG0_NJdl3d5PZU0HWUJ26hEuRYSGYKI4dTB6PrKEPVy1rDj32_gaHjfnt8

{"ticketNumber":2,"responseText":"een nieuw antwoord","isClientResponse":false}
````
response : 
````
POST https://localhost:7151/api/TicketResponses

HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Date: Sun, 24 Mar 2024 08:29:24 GMT
Server: Kestrel
Location: https://localhost:7151/api/TicketResponses?id=5
Transfer-Encoding: chunked

{
  "id": 5,
  "text": "een nieuw antwoord",
  "date": "2024-03-24T09:29:25.1214761+01:00",
  "isClientResponse": false,
  "ticketNumberOfTicket": 2
}
Response file saved.
> 2024-03-24T092925.201.json
````

## Sprint 8
```shell
dotnet test
```
resultaat van het commando:
```Starting test execution, please wait...
A total of 1 test files matched the specified pattern.

Passed!  - Failed:     0, Passed:    19, Skipped:     0, Total:    19, Duration: 7 s - Test.dll (net7.0)
```
Code Coverage : 
![Code coverage](coverage.png)

fully qualified name van de klasse waar de complexe authorization requirements afgetoetst worden :
SC.UI.Web.MVC.Controllers.TicketController

fully qualified name van de klasse waar je getest hebt met gebruik van “verification” :
Test.UnitTests.TicketControllerTest

[Tests tabblad van een pipeline waarin alle tests aanwezig waren en slaagden](https://gitlab.com/rania.zahir/supportcenter/-/pipelines/1225146273/test_report?job_name=tests)

[code coverage rapport van geslaagde tests](https://rania.zahir.gitlab.io/-/supportcenter/-/jobs/6463108542/artifacts/coveragereport/index.htm)

