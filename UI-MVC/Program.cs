using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SC.BL;
using SC.DAL;
using SC.DAL.EF;
using SC.UI.Web.MVC;

var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration.GetConnectionString("SupportCenterDbContextConnection") ?? throw new InvalidOperationException("Connection string 'SupportCenterDbContextConnection' not found.");

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddDbContext<SupportCenterDbContext>();

builder.Services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount=false)
    .AddRoles<IdentityRole>()
    .AddEntityFrameworkStores<SupportCenterDbContext>();
builder.Services.AddScoped<ITicketRepository, TicketRepository>();
builder.Services.AddScoped<ITicketManager, TicketManager>();

builder.Services.AddControllersWithViews(mvcOptions =>
    {
        mvcOptions.ReturnHttpNotAcceptable = true;
    })
    //.AddXmlSerializerFormatters()
    .AddXmlDataContractSerializerFormatters()
    .AddJsonOptions(jsonOptions =>
    {
        jsonOptions.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
    });

builder.Services.ConfigureApplicationCookie(options =>
{
    options.Events.OnRedirectToLogin += redirectContext =>
    {
        if (redirectContext.Request.Path.StartsWithSegments("/api"))
            redirectContext.Response.StatusCode = 401; // UnAuthorized
        return Task.CompletedTask;
    };
    options.Events.OnRedirectToAccessDenied += redirectContext =>
    {
        if (redirectContext.Request.Path.StartsWithSegments("/api"))
            redirectContext.Response.StatusCode = 403; // Forbidden
        return Task.CompletedTask;
    };
});

// authorization settings (oa. policies)
builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("Administrator", policyBuilder => 
        policyBuilder.RequireRole("Admin"));
    
    options.AddPolicy("TEST", policyBuilder =>
    {
        policyBuilder.RequireAssertion(ahc =>
        {
            bool isValidUser = ahc.User.IsInRole(CustomIdentityConstants.AdminRole);
            return isValidUser;
        });
    });
});

var app = builder.Build();

//ff
using (var scope = app.Services.CreateScope())
{
    SupportCenterDbContext ctx = scope.ServiceProvider.GetRequiredService<SupportCenterDbContext>();
    //ctx.Database.EnsureDeleted();
    //bool isCreated = ctx.Database.EnsureCreated(); // Code First Flow triggeren
    bool isCreated = ctx.CreateDatabase(dropDatabase: true);
    if (isCreated)
    {
        // identity seeding
        UserManager<IdentityUser> userMgr = scope.ServiceProvider.GetRequiredService<UserManager<IdentityUser>>();
        RoleManager<IdentityRole> roleMgr = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
        SeedIdentity(userMgr, roleMgr);
        // dummy data seeding
        ctx.Seed();
    }
}


//ff
/*using (var scope = app.Services.CreateScope())
{
    var context = scope.ServiceProvider.GetRequiredService<SupportCenterDbContext>();
    if (context.CreateDatabase(dropDatabase: false))
    {
        var userManager = scope.ServiceProvider
            .GetRequiredService<UserManager<IdentityUser>>();
        var roleManager = scope.ServiceProvider
            .GetRequiredService<RoleManager<IdentityRole>>();
        SeedIdentity(userManager, roleManager);

        DataSeeder.Seed(context);
    }
}*/

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapRazorPages();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();

void SeedIdentity(UserManager<IdentityUser> userManager,
    RoleManager<IdentityRole> roleManager)
{

//b
    var rania = new IdentityUser
    {
        Email = "rania@kdg.be",
        UserName = "rania@kdg.be",
        EmailConfirmed = true
    };
    userManager.CreateAsync(rania, "Rania1?").Wait();
    
    var annick = new IdentityUser
    {
        Email = "annick@kdg.be",
        UserName = "annick@kdg.be",
        EmailConfirmed = true
    };
    userManager.CreateAsync(annick, "Annick1?").Wait();

    var john = new IdentityUser{
        Email = "john@kdg.be",
        UserName = "john@kdg.be",
        EmailConfirmed = true
        };
    userManager.CreateAsync(john, "John1?").Wait();

    var sophie = new IdentityUser
    {
        Email = "sophie@kdg.be",
        UserName = "sophie@kdg.be",
        EmailConfirmed = true
    };
    userManager.CreateAsync(sophie, "Sophie1?").Wait();
    
    var kenneth = new IdentityUser
    {
        Email = "kenneth@kdg.be",
        UserName = "kenneth@kdg.be",
        EmailConfirmed = true
    };
    userManager.CreateAsync(kenneth, "Kenneth1?").Wait();
//n
//roles
    const string adminRole = "Admin";
    roleManager.CreateAsync(new IdentityRole(adminRole)).Wait();
    // admin -> adminRole
    userManager.AddToRoleAsync(kenneth, adminRole).Wait();
//n
}
public partial class Program { }

