namespace SC.UI.Web.MVC.Models.Dto;

public class NewTicketResponseDTO
{
    public int TicketNumber { get; set; }
    public string ResponseText { get; set; }
    public bool IsClientResponse { get; set; }
}