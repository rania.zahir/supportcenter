﻿using System.Diagnostics;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SC.UI.Web.MVC.Models;

namespace SC.UI.Web.MVC.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Privacy()
    {
        HardCodedLogin();
        return View();
    }
    
    private void HardCodedLogin()
    {
        var claims = new List<Claim>()
        {
            new Claim(ClaimTypes.Name, "rania@kdg.be"),
            new Claim(ClaimTypes.NameIdentifier, "389bf9e2-bd7f-4d08-9eee-234322ea67d7")
        };
        var identity = new ClaimsIdentity(claims, IdentityConstants.ApplicationScheme);
        var principal = new ClaimsPrincipal(identity);
        HttpContext.SignInAsync(IdentityConstants.ApplicationScheme, principal);
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
    }
}