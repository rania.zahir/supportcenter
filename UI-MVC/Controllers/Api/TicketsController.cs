using Microsoft.AspNetCore.Mvc;
using SC.BL;
using SC.BL.Domain;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace SC.UI.Web.MVC.Controllers.Api;

[ApiController]
[Route("api/[controller]")]
[Authorize]
public class TicketsController : Controller
{
    private readonly ITicketManager _mgr;

    public TicketsController(ITicketManager ticketManager)
    {
        _mgr = ticketManager;
    }
    
// PUT: api/Tickets/5/State/Closed
    [AllowAnonymous] // Allow unauthenticated access to this specific action
    [HttpPut("{id}/State/Closed")]
    public IActionResult PutTicketStateToClosed(int id)
    {
        // Haal het ticket op
        Ticket ticket = _mgr.GetTicket(id);

        // Controleer of de huidige gebruiker een administrator is
        bool isAdmin = User.IsInRole(CustomIdentityConstants.AdminRole);

        // Controleer of de huidige gebruiker de maintainer is
        bool isMaintainer = ticket.Maintainer?.Id == User.FindFirstValue(ClaimTypes.NameIdentifier);

        if (!isAdmin && !isMaintainer)
        {
            // Als de gebruiker geen administrator of maintainer is, ontzeg de toegang
            return Forbid();
        }

        if (ticket == null)
            return NotFound();

        // Werk de status van het ticket bij
        ticket.State = TicketState.Closed;
        _mgr.ChangeTicket(ticket);

        return NoContent();
    }
}
