using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SC.BL;
using SC.BL.Domain;
using SC.UI.Web.MVC.Models.Dto;

namespace SC.UI.Web.MVC.Controllers.Api;

[ApiController]
[Route("api/[controller]")]
public class TicketResponsesController : ControllerBase
{
    private readonly ITicketManager _mgr;

    public TicketResponsesController(ITicketManager ticketManager)
    {
        _mgr = ticketManager;
    }

    // GET: api/TicketResponses?ticketNumber=5
    [HttpGet]
    public IActionResult Get(int ticketNumber)
    {
        var responses = _mgr.GetTicketResponses(ticketNumber);

        if (responses == null || !responses.Any())
            return NoContent();

        List<TicketResponseDTO> responseDtos = new List<TicketResponseDTO>();
        foreach (var response in responses)
        {
            responseDtos.Add(new TicketResponseDTO()
            {
                Id = response.Id,
                Text = response.Text,
                Date = response.Date,
                IsClientResponse = response.IsClientResponse,
                TicketNumberOfTicket = response.Ticket.TicketNumber
            });
        }

        return Ok(responseDtos);
    }

    // POST: api/TicketResponses
    [HttpPost]
    //[Authorize(Roles = "Admin")]
   /* public IActionResult Post(NewTicketResponseDTO response)
    {
        // Check if the user has the necessary role to perform this action
        if (!User.IsInRole("Admin"))
        {
            return Unauthorized(); // or return Unauthorized();
        }

        //t
        if (response == null || !ModelState.IsValid)
        {
            return BadRequest(ModelState); // Return BadRequest with validation errors
        }
        //t
        TicketResponse createdResponse =
            _mgr.AddTicketResponse(response.TicketNumber, response.ResponseText, response.IsClientResponse);

        if (createdResponse == null)
            return BadRequest("Er is iets misgelopen bij het registreren van het antwoord!");

        TicketResponseDTO responseDto = new TicketResponseDTO()
        {
            Id = createdResponse.Id,
            Text = createdResponse.Text,
            Date = createdResponse.Date,
            IsClientResponse = createdResponse.IsClientResponse,
            TicketNumberOfTicket = createdResponse.Ticket.TicketNumber
        };

        return CreatedAtAction("Get", new { id = responseDto.Id }, responseDto);
    }*/
    
   //tr
    public IActionResult Post(NewTicketResponseDTO response)
    {
        // Check if the user has the necessary role to perform this action
        if (!User.IsInRole("Admin"))
        {
            // Return BadRequest if user is not in the "Admin" role
            return BadRequest("Unauthorized: User is not authorized to perform this action");
        }

        // Check for invalid HTTP body
        if (response == null || !ModelState.IsValid)
        {
            return BadRequest(ModelState); // Return BadRequest with validation errors
        }

        // Process valid request
        TicketResponse createdResponse =
            _mgr.AddTicketResponse(response.TicketNumber, response.ResponseText, response.IsClientResponse);

        if (createdResponse == null)
            return BadRequest("Er is iets misgelopen bij het registreren van het antwoord!");

        TicketResponseDTO responseDto = new TicketResponseDTO()
        {
            Id = createdResponse.Id,
            Text = createdResponse.Text,
            Date = createdResponse.Date,
            IsClientResponse = createdResponse.IsClientResponse,
            TicketNumberOfTicket = createdResponse.Ticket.TicketNumber
        };

        return CreatedAtAction("Get", new { id = responseDto.Id }, responseDto);
    }
}