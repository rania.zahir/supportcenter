using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SC.BL;
using SC.BL.Domain;
using SC.UI.Web.MVC.Models;

namespace SC.UI.Web.MVC.Controllers;

public class TicketController : Controller
{
    private readonly ITicketManager _mgr;
    private readonly UserManager<IdentityUser> _userManager;
    public TicketController(ITicketManager ticketManager,UserManager<IdentityUser> userManager)
    {
        _mgr = ticketManager;
        _userManager = userManager;
    }
    
    // GET: /Ticket
    public IActionResult Index()
    {
        IEnumerable<Ticket> tickets = _mgr.GetTickets();
        return View(tickets);
    }
    
    // GET: /Ticket/Details/<ticket_number>
    public IActionResult Details(int id)
    {
        Ticket ticket = _mgr.GetTicket(id);
        
        // Check if the ticket exists
        if (ticket == null)
        {
            return NotFound();
        }

        // You can now access the maintainer's information
        var maintainerName = ticket.Maintainer?.UserName;

        
        if(ticket.Responses != null)
            ViewBag.Responses = ticket.Responses;
        else
            ViewBag.Responses = _mgr.GetTicketResponses(ticket.TicketNumber);
        
        return View(ticket);
    }
    
    // GET: /Ticket/Edit/<ticket_number>
    [Authorize(Policy="Administrator")]
    public IActionResult Edit(int id)
    {
        Ticket ticket = _mgr.GetTicket(id);
        // Check if the logged-in user has the necessary role
        if (!User.IsInRole(CustomIdentityConstants.AdminRole))
        {
            // If not, check if the logged-in user is the ticket maintainer
            bool isAuthorized = ticket.Maintainer?.Id == User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (!isAuthorized)
            {
                // If neither role nor ticket maintainer match, deny access
                return RedirectToPage("/Account/AccessDenied", new { Area = "Identity" });
            }
        }

        return View(ticket);
    }

    // POST: /Ticket/Edit/<ticket_number>
    [HttpPost]
    public IActionResult Edit(int id, Ticket ticket)
    {
        if(!ModelState.IsValid)
            return View(ticket);
            
        _mgr.ChangeTicket(ticket);
        return RedirectToAction("Details", new {id = ticket.TicketNumber});
    }
    
    // GET: /Ticket/Create
    [Authorize]
    public IActionResult Create()
    {
        return View();
    }

    // POST: /Ticket/Create
    [HttpPost]
    [Authorize]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create(CreateTicketViewModel createTicket)
    {
        // Check if the user has the necessary role to perform this action
        bool isAuthorized = !User.IsInRole(CustomIdentityConstants.AdminRole);
        if (!isAuthorized)
        {
            return Forbid();
        }

        // Get the current logged-in user
        IdentityUser currentUser = await _userManager.GetUserAsync(HttpContext.User);

        //t
        if (!ModelState.IsValid)
        {
            return View(createTicket);
        }
        //t
        
        // Create a new Ticket and associate it with the current user
        Ticket newTicket = new Ticket
        {
            AccountId = createTicket.AccId,
            Text = createTicket.Problem,
            DateOpened = DateTime.Now,
            // Associate the ticket with the current user
            Maintainer = currentUser,
            State = TicketState.Open
        };

        // Add the ticket to the database
        _mgr.AddTicket(newTicket);

        return RedirectToAction("Details", new { id = newTicket.TicketNumber });
    }

    //t
    
    // GET: /Ticket/Delete/<ticket_number>
    public IActionResult Delete(int id)
    {
        Ticket ticket = _mgr.GetTicket(id);
        return View(ticket);
    }

    // POST: /Ticket/Delete/<ticket_number>
    [HttpPost]
    public IActionResult DeleteConfirmed(int id)
    {
        _mgr.RemoveTicket(id);
        return RedirectToAction("Index");
    }
}