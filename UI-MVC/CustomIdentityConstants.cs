namespace SC.UI.Web.MVC;

public class CustomIdentityConstants
{
    public const string UserRole = "user";
    public const string AdminRole = "Admin";   
}