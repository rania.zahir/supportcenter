using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using SC.BL;
using SC.BL.Domain;
using SC.UI.Web.MVC.Controllers;
using SC.UI.Web.MVC.Models;

namespace Test.UnitTests;

public class TicketControllerTest
{
    private readonly TicketController _ticketController;
    private readonly Mock<ITicketManager> _ticketManagerMock;
    private readonly Mock<UserManager<IdentityUser>> _userManagerMock;

    public TicketControllerTest()
    {
        _ticketManagerMock = new Mock<ITicketManager>();
        _userManagerMock = new Mock<UserManager<IdentityUser>>(Mock.Of<IUserStore<IdentityUser>>(), null, null, null, null, null, null, null, null);
        _ticketController = new TicketController(_ticketManagerMock.Object, _userManagerMock.Object)
        {
            ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext { User = new ClaimsPrincipal() }
            }
        };
        _ticketController.TempData = new TempDataDictionary(new DefaultHttpContext(), Mock.Of<ITempDataProvider>());
    }
    
    private Mock<UserManager<TUser>> GetMockUserManager<TUser>()
        where TUser : class
    {
        var userManagerMock = new Mock<UserManager<TUser>>(
            new Mock<IUserStore<TUser>>().Object,
            new Mock<IOptions<IdentityOptions>>().Object,
            new Mock<IPasswordHasher<TUser>>().Object,
            new IUserValidator<TUser>[0],
            new IPasswordValidator<TUser>[0],
            new Mock<ILookupNormalizer>().Object,
            new Mock<IdentityErrorDescriber>().Object,
            new Mock<IServiceProvider>().Object,
            new Mock<ILogger<UserManager<TUser>>>().Object);

        return userManagerMock;
    }
    
    //CREATE
    //test
    [Fact]
    public async Task Create_WithValidData_ShouldAddTicketAndRedirectToDetails()
    {
        // Arrange
        var createTicketViewModel = new CreateTicketViewModel
        {
            AccId = 1,
            Problem = "Test problem"
        };
        var currentUser = new IdentityUser();
        _userManagerMock.Setup(m => m.GetUserAsync(It.IsAny<ClaimsPrincipal>())).ReturnsAsync(currentUser);
        _ticketManagerMock.Setup(m => m.AddTicket(It.IsAny<Ticket>())).Callback<Ticket>(ticket =>
        {
            ticket.TicketNumber = 123; // Simulate database-generated ticket number
        });

        // Act
        var result = await _ticketController.Create(createTicketViewModel);

        // Assert
        _ticketManagerMock.Verify(m => m.AddTicket(It.IsAny<Ticket>()), Times.Once);
        var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
        Assert.Equal("Details", redirectToActionResult.ActionName);
        Assert.Equal(123, redirectToActionResult.RouteValues["id"]); // Expected ticket number after creation
    }

    // DETAILS METHODS

    [Fact]
    public void Details_WithValidTicketId_ShouldReturnViewWithTicket()
    {
        // Arrange
        var ticket = new Ticket { TicketNumber = 1, Text = "Test ticket" };
        _ticketManagerMock.Setup(mgr => mgr.GetTicket(1)).Returns(ticket);

        // Act
        var result = _ticketController.Details(1);

        // Assert
        var viewResult = Assert.IsType<ViewResult>(result);
        var model = Assert.IsAssignableFrom<Ticket>(viewResult.Model);
        Assert.Equal(ticket, model);
    }

    [Fact]
    public void Details_WithInvalidTicketId_ShouldReturnNotFound()
    {
        // Arrange
        _ticketManagerMock.Setup(mgr => mgr.GetTicket(1)).Returns((Ticket)null);

        // Act
        var result = _ticketController.Details(1);

        // Assert
        Assert.IsType<NotFoundResult>(result);
    }
    
    //EDIT METHODES 
    //done
    [Fact]
    public void Edit_WithValidModelState_ShouldPersistTicketAndRedirectToDetails()
    {
        // Arrange
        var ticket = new Ticket { TicketNumber = 1 };
        _ticketController.ModelState.Clear(); // Clear ModelState to simulate valid state
        _ticketManagerMock.Setup(mgr => mgr.ChangeTicket(It.IsAny<Ticket>())).Verifiable();

        // Act
        var result = _ticketController.Edit(1, ticket);

        // Assert
        _ticketManagerMock.Verify(mgr => mgr.ChangeTicket(ticket), Times.Once);
        var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
        Assert.Equal("Details", redirectToActionResult.ActionName);
        Assert.Equal(1, redirectToActionResult.RouteValues["id"]);
    }
    
    //done
    [Fact]
    public void Edit_WithInvalidModelState_ShouldReturnView()
    {
        // Arrange
        var ticket = new Ticket();
        _ticketController.ModelState.AddModelError("Ticket", "Required");

        // Act
        var result = _ticketController.Edit(1, ticket);

        // Assert
        var viewResult = Assert.IsType<ViewResult>(result);
        Assert.Equal(ticket, viewResult.Model);
    }
}
