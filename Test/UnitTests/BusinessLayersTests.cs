using System.ComponentModel.DataAnnotations;
using Moq;
using SC.BL;
using SC.BL.Domain;
using SC.DAL;

namespace Test.UnitTests;

public class BusinessLayersTests
{
    private readonly ITicketManager _manager;
    private readonly Mock<ITicketRepository> _repositoryMock;

    public BusinessLayersTests()
    {
        _repositoryMock = new Mock<ITicketRepository>();
        _manager = new TicketManager(_repositoryMock.Object);
    }

    //done
    [Fact]
    public void AddTicketResponse_WithInvalidData_ShouldThrowValidationException()
    {
        // Arrange
        var ticketNumber = 1;
        var response = ""; // invalid
        var isClientResponse = true;

        _repositoryMock.Setup(r => r.ReadTicket(ticketNumber))
            .Returns((Ticket)null);

        // Act
        void action() => _manager.AddTicketResponse(ticketNumber, response, isClientResponse);

        // Assert
        Assert.Throws<ArgumentException>(action);
    }
    
    //done
    [Fact]
    public void AddTicketResponse_WithValidData_ShouldAddResponse()
        {
            // Arrange
            var ticketNumber = 1;
            var response = "Test response";
            var isClientResponse = false;

            var ticketManager = new TicketManager(Mock.Of<ITicketRepository>());

            var ticket = new Ticket
            {
                TicketNumber = ticketNumber,
                AccountId = 1,
                Text = "Test ticket",
                DateOpened = DateTime.Now,
                State = TicketState.Open,
                Responses = new List<TicketResponse>()
            };

            var ticketResponse = new TicketResponse
            {
                Text = response,
                IsClientResponse = isClientResponse
            };

            var ticketRepositoryMock = new Mock<ITicketRepository>();
            ticketRepositoryMock.Setup(r => r.ReadTicket(ticketNumber)).Returns(ticket);
            ticketRepositoryMock.Setup(r => r.ReadTicketResponsesOfTicket(ticketNumber)).Returns(new List<TicketResponse>());
            ticketRepositoryMock.Setup(r => r.CreateTicketResponse(It.IsAny<TicketResponse>())).Returns(ticketResponse);

            var ticketManagerWithMockRepository = new TicketManager(ticketRepositoryMock.Object);

            // Act
            var addedResponse = ticketManagerWithMockRepository.AddTicketResponse(ticketNumber, response, isClientResponse);

            // Assert
            Assert.NotNull(addedResponse);
            Assert.Equal(response, addedResponse.Text);
            Assert.Equal(isClientResponse, addedResponse.IsClientResponse);
            Assert.Equal(ticketNumber, addedResponse.Ticket.TicketNumber);
            Assert.Contains(addedResponse, ticket.Responses);
            ticketRepositoryMock.Verify(r => r.ReadTicket(ticketNumber), Times.Once);
            ticketRepositoryMock.Verify(r => r.ReadTicketResponsesOfTicket(ticketNumber), Times.Once);
            ticketRepositoryMock.Verify(r => r.CreateTicketResponse(It.IsAny<TicketResponse>()), Times.Once);
            ticketRepositoryMock.Verify(r => r.UpdateTicket(It.IsAny<Ticket>()), Times.Once);
        }
    
    //done
    [Fact]
    public void AddTicket_WithInvalidData_ShouldThrowValidationException()
    {
        // Arrange
        var mockRepo = new Mock<ITicketRepository>();
        mockRepo.Setup(r => r.CreateTicket(It.IsAny<Ticket>()))
            .Verifiable(Times.Never);

        var mgr = new TicketManager(mockRepo.Object);
        int accountId = 1;
        string device = "ABC"; // valid
        string problem = ""; // invalid

        // Act
        var action = () =>
        {
            mgr.AddTicket(accountId, device, problem);
        };

        // Assert
        Assert.Throws<ValidationException>(action);
        mockRepo.VerifyAll();
    }
    
    //done
    [Fact]
    public void AddTicket_WithValidData_ShouldAddTicket()
    {
        // Arrange
        var accountId = 1;
        var question = "Test question"; // valid

        var ticket = new Ticket
        {
            AccountId = accountId,
            Text = question,
            DateOpened = DateTime.Now,
            State = TicketState.Open
        };

        _repositoryMock.Setup(r => r.CreateTicket(It.IsAny<Ticket>()))
            .Returns(ticket)
            .Verifiable();

        // Act
        var addedTicket = _manager.AddTicket(accountId, question);

        // Assert
        Assert.NotNull(addedTicket);
        Assert.Equal(accountId, addedTicket.AccountId);
        Assert.Equal(question, addedTicket.Text);
        _repositoryMock.Verify(r => r.CreateTicket(It.IsAny<Ticket>()), Times.Once);
    }
}
