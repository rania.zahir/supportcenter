using System.ComponentModel.DataAnnotations;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using SC.BL;
using SC.BL.Domain;
using SC.DAL;
using SC.DAL.EF;

namespace Test;

public class BusinessLayerTest : IDisposable
{
        private readonly TicketManager _mgr;
        private readonly SqliteConnection _connection;
        private readonly SupportCenterDbContext _ctx;

    //test setup 
    public BusinessLayerTest()
        {
            _connection = new SqliteConnection(@"Data Source=:memory:");
            _connection.Open();
            DbContextOptionsBuilder optionsBuilder = new DbContextOptionsBuilder<SupportCenterDbContext>();
            optionsBuilder.UseSqlite(_connection);
            _ctx = new SupportCenterDbContext(optionsBuilder.Options);
            
            // Ensure the database is created and migrations are applied
            _ctx.Database.EnsureCreated();
            
            ITicketRepository repo = new SC.DAL.EF.TicketRepository(_ctx);
            _mgr = new TicketManager(repo);
        }
        
	//test cleanup
    public void Dispose()
    {
        _ctx.Database.EnsureDeleted();
        _connection.Close();
        _ctx?.Dispose();
    }
    
    [Fact]
    public void ChangeTicket_UpdatesTicketInRepository()
    {
        // Arrange
        // Create a sample ticket for testing
        var originalTicket = new Ticket
        {
            TicketNumber = 1,
            AccountId = 123,
            Text = "Sample Question",
            DateOpened = DateTime.Now,
            State = TicketState.Open
            // Add other properties as needed
        };

        // Add the sample ticket to the repository
        _ctx.Tickets.Add(originalTicket);
        _ctx.SaveChanges();

        // Act
        // Call the ChangeTicket method
        _mgr.ChangeTicket(originalTicket);

        // Assert
        // Retrieve the ticket from the repository after the change
        var updatedTicket = _ctx.Tickets.Find(originalTicket.TicketNumber);

        // Perform assertions
        Assert.NotNull(updatedTicket); // Ensure the ticket was found
        Assert.Equal(TicketState.Open, updatedTicket.State); // Perform other assertions as needed
    }

    [Fact]
    public void AddTicket_CreatesTicketInRepository()
    {
        // Arrange
        var accountId = 123;
        var question = "Sample Question";

        // Act
        // Call the AddTicket method
        var addedTicket = _mgr.AddTicket(accountId, question);

        // Assert
        // Retrieve the ticket from the repository
        var retrievedTicket = _ctx.Tickets.Find(addedTicket.TicketNumber);

        // Perform assertions
        Assert.NotNull(retrievedTicket); // Ensure the ticket was found
        Assert.Equal(accountId, retrievedTicket.AccountId); // Perform other assertions as needed
        Assert.Equal(question, retrievedTicket.Text);
        Assert.Equal(TicketState.Open, retrievedTicket.State);
    }
    
    [Fact]
    public void AddTicket_WithInvalidData_ThrowsValidationException()
    {
        // Arrange
        var accountId = 0; // Invalid account ID
        string question = null; // Invalid null question

        // Act & Assert
        // Call the AddTicket method with invalid data and expect a ValidationException
        Assert.Throws<ValidationException>(() => _mgr.AddTicket(accountId, question));
    }

}

