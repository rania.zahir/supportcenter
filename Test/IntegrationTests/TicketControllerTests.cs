using System.Net;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using SC.UI.Web.MVC;
using SC.UI.Web.MVC.Models;
using Test.IntegrationTests.Config;

namespace Test;

public class TicketControllerTests : IClassFixture<CustomWebApplicationFactoryWithMockAuth<Program>>
{
    private readonly CustomWebApplicationFactoryWithMockAuth<Program> _factory;

    public TicketControllerTests(CustomWebApplicationFactoryWithMockAuth<Program> factory)
    {
        _factory = factory;
    }

    //done
    [Fact]
    public async Task Create_Ticket_Without_Authorization_Should_Return_Redirect()
    {
        // Arrange
        var client = _factory.CreateClient(
            new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });

        // Act
        var response = await client.PostAsync("/Ticket/Create", null);

        // Assert
        Assert.Equal(HttpStatusCode.Redirect, response.StatusCode);
        Assert.Equal("/Identity/Account/Login", response.Headers.Location?.AbsolutePath);
    }
    //done M
    [Fact]
    public void Create_Ticket_Should_Return_View_With_ModelState_Errors()
    {
        // Arrange
        var client = _factory
            .AuthenticatedInstance(
                new Claim(ClaimTypes.Name, "kenneth@kdg.be"))
            .CreateClient(
                new WebApplicationFactoryClientOptions
                {
                    AllowAutoRedirect = false
                });

        // Act
        var response = client.PostAsync("/Ticket/Create",
            new FormUrlEncodedContent(new Dictionary<string, string>
            {
                { "AccId", "7" },
                { "Problem", "Test problem" }
            })).Result;
        var content = response.Content.ReadAsStringAsync().Result;

        // Assert
        Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        Assert.DoesNotContain(">The Problem field is required.<", content);
    }

    //done
    [Fact]
    public async Task Edit_Ticket_As_Administrator_Should_Succeed()
    {
        // Arrange
        var client = _factory
            .AuthenticatedInstance(
                new Claim(ClaimTypes.Name, "kenneth@kdg.be"), // Assuming this user has Administrator role
                new Claim(ClaimTypes.Role, CustomIdentityConstants.AdminRole)) // Assigning Administrator role
            .CreateClient(
                new WebApplicationFactoryClientOptions
                {
                    AllowAutoRedirect = false
                });

        // Act
        var response = await client.GetAsync("/Ticket/Edit/1"); // Assuming ticket ID 1 exists

        // Assert
        Assert.Equal(HttpStatusCode.OK, response.StatusCode);
    }
    
    [Fact]
    public async Task Edit_Ticket_As_Non_Administrator_Should_Return_Forbidden()
    {
        // Arrange
        var client = _factory
            .AuthenticatedInstance(
                new Claim(ClaimTypes.Name, "annick@kdg.be")) // Assuming this user does not have Administrator role
            .CreateClient(
                new WebApplicationFactoryClientOptions
                {
                    AllowAutoRedirect = false
                });

        // Act
        var response = await client.GetAsync("/Ticket/Edit/2"); // Assuming ticket ID 1 exists

        // Assert
        Assert.Equal(HttpStatusCode.Forbidden, response.StatusCode);
    }
}
