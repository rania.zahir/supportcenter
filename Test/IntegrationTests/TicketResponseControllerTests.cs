using System.Net;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using SC.BL;
using SC.UI.Web.MVC.Models.Dto;
using Test.IntegrationTests.Config;

namespace Test;

public class TicketResponseControllerTests : IClassFixture<CustomWebApplicationFactoryWithMockAuth<Program>>
{
    private readonly CustomWebApplicationFactoryWithMockAuth<Program> _factory;

    public TicketResponseControllerTests(CustomWebApplicationFactoryWithMockAuth<Program> factory)
    {
        _factory = factory;
    }
    
    [Fact]
    public async void Post_TicketResponse_Should_Return_Created_For_Admin_User()
    {
        // Arrange
        var httpClient = _factory
            .AuthenticatedInstance(
                new Claim(ClaimTypes.Role, "Admin")
            )
            .CreateClient(
                new WebApplicationFactoryClientOptions
                {
                    AllowAutoRedirect = false
                });

        var newTicketResponse = new NewTicketResponseDTO
        {
            TicketNumber = 1, // Adjust according to your test scenario
            ResponseText = "Test response",
            IsClientResponse = false
        };

        var jsonContent = JsonSerializer.Serialize(newTicketResponse);
        var httpContent = new StringContent(jsonContent, Encoding.UTF8, "application/json");

        // Act
        var response = await httpClient.PostAsync("/api/TicketResponses", httpContent);

        // Assert
        Assert.Equal(HttpStatusCode.Created, response.StatusCode);
    }

     [Fact]
     public async Task Post_TicketResponse_Should_Return_BadRequest_For_Invalid_Http_Body()
  {
      // Arrange
      var httpClient = _factory
          .AuthenticatedInstance(
              new Claim(ClaimTypes.Name, "kenneth@kdg.be")
          )
          .CreateClient(
              new WebApplicationFactoryClientOptions
              {
                  AllowAutoRedirect = false
              });
    
      var invalidTicketResponse = new NewTicketResponseDTO
      {
          // Populate invalid ticket response DTO with missing required fields
          ResponseText = "",
          IsClientResponse = false
      };

      var jsonContent = JsonSerializer.Serialize(invalidTicketResponse);
      var httpContent = new StringContent(jsonContent, Encoding.UTF8, "application/json");

      // Act
      var response = await httpClient.PostAsync("/api/TicketResponses", httpContent);

      // Assert
      Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
  }
     
     
     [Fact]
     public async Task Post_TicketResponses_Should_Return_Created_For_Valid_Http_Body()
     {
         // Arrange
         var httpClient = _factory
             .AuthenticatedInstance(
                 new Claim(ClaimTypes.Name, "kenneth@kdg.be"),
                 new Claim(ClaimTypes.Role, "Admin")
             )
             .CreateClient(
                 new WebApplicationFactoryClientOptions
                 {
                     AllowAutoRedirect = false
                 });

         var validTicketResponse = new NewTicketResponseDTO
         {
             TicketNumber = 1, // Adjust according to your test scenario
             ResponseText = "Test response",
             IsClientResponse = false
         };

         var jsonContent = JsonSerializer.Serialize(validTicketResponse);
         var httpContent = new StringContent(jsonContent, Encoding.UTF8, "application/json");

         // Act
         var response = await httpClient.PostAsync("/api/TicketResponses", httpContent);

         // Assert
         Assert.Equal(HttpStatusCode.Created, response.StatusCode);
     }
    
}
